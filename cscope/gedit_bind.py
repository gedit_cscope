import gedit
import os
import gtk
import gtk.glade
import cscope

GLADE_FILE = os.path.join(os.path.dirname(__file__), "cscope.glade")

[ COL_ID_ID,  COL_TEXT_ID, COL_TYPE_ID ]            = [ 0, "#",         "text" ]
[ COL_ID_DIR, COL_TEXT_DIR, COL_TYPE_DIR ]          = [ 1, "Directory", "text" ]
[ COL_ID_FILE, COL_TEXT_FILE, COL_TYPE_FILE ]       = [ 2, "File",      "text" ]
[ COL_ID_LINE, COL_TEXT_LINE, COL_TYPE_LINE ]       = [ 3, "Line#",     "text" ]
[ COL_ID_SYMBOL, COL_TEXT_SYMBOL, COL_TYPE_SYMBOL ] = [ 4, "Symbol",    "text" ]
[ COL_ID_TEXT, COL_TEXT_TEXT, COL_TYPE_TEXT ]       = [ 5, "Text",      "text" ]



class CScopeResult:
	def __init__(self, result):
		self._result = result
		self._search_data = gtk.ListStore(int, str, str, int, str, str)
		self._result.set_model(self._search_data)
		tree_selection = self._result.get_selection()
        	tree_selection.set_mode(gtk.SELECTION_SINGLE)
        	
        	self.addColumn(COL_TEXT_ID, COL_ID_ID, COL_TYPE_ID)
		self.addColumn(COL_TEXT_DIR, COL_ID_DIR, COL_TYPE_DIR)
		self.addColumn(COL_TEXT_FILE, COL_ID_FILE, COL_TYPE_FILE)
		self.addColumn(COL_TEXT_LINE, COL_ID_LINE, COL_TYPE_LINE)
		self.addColumn(COL_TEXT_SYMBOL, COL_ID_SYMBOL, COL_TYPE_SYMBOL)
		self.addColumn(COL_TEXT_TEXT, COL_ID_TEXT, COL_TYPE_TEXT)
        	
        	self._result.set_headers_visible(True)
        	self._result.show()

		
	def addColumn(self, name, colid, coltype):
		cell = gtk.TreeViewColumn(name)
		cell.set_resizable(True)
		cell.set_reorderable(True)
		cell.set_sort_column_id(colid)
		
		self._result.append_column(cell)
		
		text_renderer = gtk.CellRendererText()
		        
		# Pack the text renderer objects into the cell objects we created
		cell.pack_start(text_renderer, True)
        
		# Now set the IDs to each of the text renderer objects and set them to "text" mode
		cell.add_attribute(text_renderer, coltype, colid)

	def clear(self):
		self._search_data.clear()
		
	def append(self, data):
		self._search_data.append(data)

class CScopePluginHelper:
	def __init__(self, plugin, window):
		self._plugin = plugin
		self._window = window
		self._panel  = self._window.get_bottom_panel()
		self._cspanel = None
		self.create_panel()
		self._cscope = cscope.CScope()
		return

	def deactivate(self):
		self._plugin = None
		self._window = None
		self._panel  = None
		self._cspanel = None
	
	def update_ui(self):
		return
		
	def create_panel(self):
		# Open the Interface and bind with Botton Panel
		glade_xml = gtk.glade.XML(GLADE_FILE, "CScopeBox")
		self._cspanel = glade_xml.get_widget("CScopeBox")
		image = gtk.Image()
		image.set_from_stock(gtk.STOCK_DND_MULTIPLE, gtk.ICON_SIZE_BUTTON)
		self._panel.add_item(self._cspanel, "CScope", image)
		
		# Objects
		self._fcDatabase = glade_xml.get_widget("fcDatabase")
		self._bOpen = glade_xml.get_widget("bOpen")
		self._bSearch = glade_xml.get_widget("bSearch")
		self._eQuery = glade_xml.get_widget("eQuery")
		self._cbTypeOfQuery = glade_xml.get_widget("cbTypeOfQuery")
		self._tvResults = glade_xml.get_widget("tvResult")

		self._cbTypeOfQuery.set_active(0)

		self._bSearch.connect("clicked", self.do_search)
		self._bOpen.connect("clicked", self.do_opendb)

		self._result = CScopeResult(self._tvResults)

		return

	def do_search(self, button):
		ris = self._cscope.query(str(self._cbTypeOfQuery.get_active()), self._eQuery.get_text())
		self._result.clear()
		i = 0
		for rec in ris:
			[filename, funcname, rowno, text] = rec
			self._result.append(
				( i,
				  os.path.dirname(filename),
				  os.path.basename(filename),
				  rowno,
				  funcname,
				  text ))	
			i = i + 1
		return

	def do_opendb(self, button):
		self._cscope.shutdown()
		self._cscope.setDb(self._fcDatabase.get_filename())
		self._cscope.setHome(os.path.dirname(self._fcDatabase.get_filename()))
		self._cscope.start()
		return
