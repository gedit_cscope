import os
import re

# CScope comands mnemonic
CSCOPE_FINDSYMBOL   = '0' # Find this C symbol:
CSCOPE_FINDGLOBAL   = '1' # Find this global definition:
CSCOPE_FINDCALLED   = '2' # Find functions called by this function:
CSCOPE_FINDCALLER   = '3' # Find functions calling this function:
CSCOPE_FINDTEXT     = '4' # Find this text string:
CSCOPE_CHANGETEXT   = '5' # Change this text string:
CSCOPE_FINDEGREP    = '6' # Find this egrep pattern:
CSCOPE_FINDFILE     = '7' # Find this file:
CSCOPE_FINDINCLUDER = '8' # Find files #including this file:

CSCOPE_MATCH  = re.compile('^(?P<filename>(\S|\\\ )*)\ (?P<funcname>(\S*|<unknown>))\ (?P<rowno>(\d*|<unknown>))\ (?P<text>.*)$')
CSCOPE_PROMPT = re.compile('^(?P<prompt>(\>\>|))\ *(?P<cscope>(cscope:|))\ *(?P<lines>\d*).*$')

CSCOPE_QUIT       = 'quit'
CSCOPE_REBUILD    = 'r'
CSCOPE_TOGGLECASE = 'c'

class CScope:
	def __init__(self):
		# Executable
		self._exe = '/usr/bin/cscope'
		self._param = '-l'
		self._running = 0
		self._db = 'cscope.out'
		return

	def setHome(self, path):
		if self._running == 1:
			self.shutdown()
		print "Path" + path
		os.chdir(path)
		return

	def setDb(self, path):
		if self._running == 1:
			self.shutdown()
		self._db = path
		return

	def shutdown(self):	
		if self._running == 1:
			# cscope is running...
			self._sendcmd(CSCOPE_QUIT)
			# now is off
		self._running = 0
		return
		
	def start(self):
		if self._running == 1:
			self.shutdown()
		(self.csout, self.csin) = os.popen2(self._exe + ' -f' + self._db + ' ' + self._param)
		print self._exe + ' -f' + self._db + ' ' + self._param
		if (self.csout == None or self.csin == None):
			return -1
		self._running = 1
		return 0

	def query(self, cmd, symbol):
		if self._running != 1:
			return -1
		self._sendcmd(cmd+symbol)
		# Find lines
		lines = -1
		while lines < 0:
			print 'Ciclo %d' % lines
			line = self.csin.readline()
			print 'Analizzo %s' % line
			parsed = CSCOPE_PROMPT.search(line)
			if parsed.group('cscope') == 'cscope:':
				lines = int(parsed.group('lines'))
			else:
				print 'Impossibile capire ' + line				

		print 'Bisogna leggere %d linee' % lines
		result = []
		for  i in range(lines):
			line = self.csin.readline()
			parsed = CSCOPE_MATCH.search(line)
			result.append([
				parsed.group('filename'),
				parsed.group('funcname'),
				int(parsed.group('rowno')),
				parsed.group('text')
			])
		self.csin.flush()
		self.csout.flush()
		return result

	def _sendcmd(self, cmd):
		if self._running != 1:
			self.start()
		self.csout.write(cmd)
		self.csout.write('\n')
		self.csout.flush()

	def setExecutable(self, cmd, param):
		self._exe = cmd
		self._param = param

